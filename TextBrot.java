import java.io.*;
import java.util.*;

public class TextBrot {

	private static final double DEFAULT_MINX = -2.5;
    private static final double DEFAULT_MAXX = 1;
    private static final double DEFAULT_MINY = -1.25;
   	private static final double DEFAULT_MAXY = 1.25;

	private static int MAX_ITERATIONS = 1000;


	public void generateFractal(int width, int height, double minx, double maxx, double miny, double maxy) {
        
  
        
		Random r = new Random();
		int randSent = r.nextInt(50);
		int sentenceLength = r.nextInt(15);
		int wordCount = 0;
		
		String last = null;
		
		
        //for each pixel in the screen
        for (int x = 0; x < width; x++) {
			randSent = r.nextInt(50);
			wordCount = 0;
            for (int y = 0; y < height; y++) {
				wordCount ++;
				if (wordCount > sentenceLength) {
					y = height;
					sentenceLength = r.nextInt(15) + 3;
					System.out.println(".\n");
				}
			
			
				double rand = r.nextDouble() * (maxx - minx);
			
                double xco = minx + x * ((maxx + Math.abs(minx)) / width);
                double yco = miny + y * ((maxy + Math.abs(miny)) / height);
                double z = 0;
                double c = 0;
                int iterations = 0;
		
		
                //calculate escape orbit
                while ((z <= 2) && (c <= 2) && (iterations < MAX_ITERATIONS)) {
                    double temp = (z * z) - (c * c) + xco;
                    c = (2 * z * c) + yco;
                    z = temp;
                    iterations++;
                }
               
                //color the fractal
                if (iterations >= MAX_ITERATIONS) {  
                    //Iterior
					//System.out.print(".");
                } else {    
                   //Exterior color
				  
				   String text = (String) hashMap.get(iterations * randSent % hashMap.size());
				   try {
						if (!(text.equals(last))) {
							System.out.print(text + " ");
						}
						last = text;
					} catch (NullPointerException e) {
						
					}
				   
                }
            }
			
        }
}
 

	
	HashMap hashMap;

	public void readTextFile() {
	
	
		File file = new File("word_frequency.txt");
		BufferedReader reader = null;
		
		hashMap = new HashMap();
		
 
		try {
			reader = new BufferedReader(new FileReader(file));
			String text = null;
 

			while ((text = reader.readLine()) != null) {
				
				String[] content = text.split(" ");
				hashMap.put(Integer.valueOf(content[0]), content[2]);	
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		      
        Iterator iterator = hashMap.keySet().iterator();
       

        iterator = hashMap.entrySet().iterator();
	}
 



	public TextBrot() {
		readTextFile();
		
		Random r = new Random();
		
		double rmaxx = r.nextDouble() * (DEFAULT_MAXX + Math.abs(DEFAULT_MINX) + DEFAULT_MINX);
		double rmaxy = r.nextDouble() * (DEFAULT_MAXY + Math.abs(DEFAULT_MINY) + DEFAULT_MINY);
		double range = 0;
		
		if (DEFAULT_MAXX - rmaxx > DEFAULT_MAXY - rmaxy) {
			range = r.nextDouble() * (DEFAULT_MAXX - rmaxy);
		} else {
			range = r.nextDouble() * (DEFAULT_MAXY - rmaxy);
		}
		
		double rminx = rmaxx - range;
		double rminy = rmaxy - range;
		
		//System.out.println(rminx + ", " + rmaxx + "|" + rminy + ", " + rmaxy);
		
		generateFractal(10, 10, rmaxx, rminx, rmaxy, rminy);
		System.out.println("\n\n");
	}
	
	public static void main(String[] args) {
		new TextBrot();
	}


}
